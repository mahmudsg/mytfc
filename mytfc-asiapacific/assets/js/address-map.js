var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val().length ==0 ? $("[id$='_CommunityEventLocation_txtText']").attr("placeholder"):$("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val().length ==0 ? $("[id$='_CommunityEventAddress_txtText']").attr("placeholder") : $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}var map;
var infowindow;
var marker;

var map_canvas = $("#map-canvas");

function initialize() {
debugger;
    var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
    
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = "";
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();

    var lat_value = 3.0833;
    var lot_value = 101.65;
    

    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(lat_value, lot_value)
    };

    var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat_value, lot_value),
        map: map
    });

    infowindow.open(map, marker);
    
    if($('#default_map_address').length>0){
        var default_map_address = $("#default_map_address").val();
        changeMarkerLocation(default_map_address);
    }
    if($('#default_map_caption').length>0){
        var default_map_caption = $("#default_map_caption").val();
        changeInfoWindowContent(default_map_caption);
    }
   
}


$(window).load(function () {
    setTimeout(function(){
        if($('#default_map_address').length>0){
            var default_map_address = $("#default_map_address").val();
            changeMarkerLocation(default_map_address);
        }
        if($('#default_map_caption').length>0){
            var default_map_caption = $("#default_map_caption").val();
            changeInfoWindowContent(default_map_caption);
        }
    }, 2000);
}); /* end windows.load() */

function changeMarkerLocation($a) {
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
	
	if($a != ''){
		var address = $a;
		
	}else{
		var address = street_value + ',' + city_value + ',' + state_county + ',' + country + ',' + zip_value;
	}
    

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (data, status) {
        if (status == "OK") {
            var suggestion = data[0];
            var location = suggestion.geometry.location;
            console.debug(location);
            var latLng = new google.maps.LatLng(location.lat(), location.lng());

            marker.setPosition(latLng);
            map.setCenter(latLng);
			
			if (suggestion.geometry.viewport) 
                map.fitBounds(suggestion.geometry.viewport);
        }
    });
}

function changeInfoWindowContent($a) {
	var title_value = $("[id$='_CommunityEventLocation_txtText']").val();
	var street_value = $("[id$='_CommunityEventAddress_txtText']").val();
    var city_value = $("[id$='_CommunityEventCity_txtText']").val();
    var state_county = $("[id$='_CommunityEventState_txtText']").val();
    var country = $("[id$='_CommunityEventCountry_dropDownList'] :selected").val();
    var zip_value = $("[id$='_CommunityEventZipCode_txtText']").val();
    
    if($a != ''){
		var contentString = $a;
	}else{
		var contentString = '<strong>' + title_value + '</strong><br>' +
        street_value + ', ' + city_value + '<br>' +
        state_county + ' ' + zip_value + '<br>' + country + '<br><br>';
	}

    infowindow.setContent(contentString);
}


if(map_canvas.length>0){
    google.maps.event.addDomListener(window, 'load', initialize);
    //initSliders(map, marker);
}



$("[id$='_CommunityEventLocation_txtText']").change(function () {
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventAddress_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCity_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventState_txtText']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventCountry_uniSelectorCountry_drpSingleSelect :selected']").change(function () {
    changeMarkerLocation('');
    changeInfoWindowContent('');
});

$("[id$='_CommunityEventZipCode_txtText']").change(function () {
    changeMarkerLocation('');
	changeInfoWindowContent('');
});





function changeMapType(map, mapTypeId) {
    map.setMapTypeId(mapTypeId);
}

function changeMapZoom(map, zoom) {
    zoom = zoom * 1;
    map.setZoom(zoom);
}