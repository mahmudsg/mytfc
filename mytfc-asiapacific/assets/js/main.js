var $ = jQuery;
$(document).ready(function() {
    $("img[data-original]").lazyload({
        threshold: 200,
        effect: "fadeIn"
    });
    $('.jk_tooltip').tooltipster({
        interactive: true,
    });
    same_height();
    fluidvids.init({
        selector: ['iframe'],
        players: ['www.youtube.com', 'player.vimeo.com']
    });
    $(".pick-date").datepicker({
        dateFormat: "mm-dd-yy",
    });
    $('.pick-time').timepicker({
        timeFormat: 'hh:mm tt'
    });
    $(".pick-date, .pick-time").focus(function(event) {
        var $this = $(this);
        if ($('.is_desktop').is(':visible')) {
            $this.prop('readonly', false);
            return;
        }
        if ($this.hasClass('active')) {
            $this.prop('readonly', false);
        } else {
            $this.addClass('active');
        }
        return;
    });
    $(".pick-date, .pick-time").focusout(function(event) {
        if ($('.is_desktop').is(':visible')) {
            return false;
        }
        var $this = $(this);
        $this.removeClass('active');
        $this.prop('readonly', true);
    });
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    $('[data-toggle="tooltip"]').tooltip();
})
$(window).load(function() {
    same_height();
})

function same_height() {
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el, topPosition = 0;
    $('.same-height').find('.shight').each(function() {
        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;
        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0;
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

/* ==============================================
========== module 1 [header] ===========
============================================== */
var header_height;
$(document).ready(function() {
    set_body_padding();
    $('#search-open').click(function(event) {
        event.preventDefault();
        var $this = $(this);
        $this.prev('.box').show("slide", {
            direction: "right"
        }, 500);
        $this.prev('.box').find('input[type=text]').focus();
        $('.dim-bg-one').fadeIn(500);
        setTimeout(function() {
            $this.css({
                'z-index': 99
            });
        }, 500);
        $('#mobile-sidebar').hide("slide", {
            direction: "left"
        }, 500);
    });
    $('#search-close').click(function(event) {
        event.preventDefault();
        $(this).parents('.box').hide("slide", {
            direction: "right"
        }, 500);
        $('#search-open').css({
            'z-index': 101
        });
        $('.dim-bg-one').fadeOut(500);
    });
    $('.main-nav-desktop li').mouseenter(function() {
        $(this).find('>ul').stop().animate({
            top: '100%'
        }, 500);
    });
    $('.main-nav-desktop li').mouseleave(function() {
        var a = $(this).find('>ul').height();
        a += Number(20);
        $(this).find('>ul').stop().animate({
            top: '-' + a
        }, 500);
    });
    $('#global-menu>a').click(function(event) {
        event.preventDefault();
        if ($(this).parent('li').find('>div').hasClass('opened')) {
            var a = $(this).parent('li').find('>div').height();
            a += Number(20);
            $(this).parent('li').find('>div').stop().animate({
                top: '-' + a
            }, 500);
            $(this).parent('li').find('>div').removeClass('opened');
        } else {
            $(this).parent('li').find('>div').stop().animate({
                top: '100%'
            }, 500);
            $(this).parent('li').find('>div').addClass('opened');
        }
    });
    $('#global-menu').mouseleave(function() {
        var a = $(this).find('>div').height();
        a += Number(20);
        $(this).find('>div').stop().animate({
            top: '-' + a
        }, 500);
        $(this).find('>div').removeClass('opened');
    });
    $('.mmenu-button').click(function(event) {
        $('#mobile-sidebar').show("slide", {
            direction: "left"
        }, 500);
        $('.dim-bg-one').fadeIn(500);
    });
    $('.dim-bg-one').click(function(event) {
        event.preventDefault();
        close_all();
    });
    $(document).on('click', '.dim-bg-one', function(event) {});
    $('.main-nav-mobile .has-submenu').mouseenter(function() {
        $(this).find('>ul').stop().slideDown(500);
        $(this).find('>div').stop().slideDown(500);
        $(this).addClass('opned');
    });
    $('.main-nav-mobile .has-submenu').mouseleave(function() {
        $(this).find('>ul').stop().slideUp(500);
        $(this).find('>div').stop().slideUp(500);
        $(this).removeClass('opned');
    });
})
$(window).load(function() {
    set_body_padding();
    hide_header_scroll();
    if ($('.is_desktop').is(":visible")) {
        $('.main-nav-desktop li>ul').each(function() {
            $(this).css('top', '-' + $(this).height() + 'px');
        });
        $('#global-menu>div').each(function() {
            $(this).css('top', '-' + $(this).height() + 'px');
        });
    }
});
$(window).on('resize', function() {
    set_body_padding();
});

function set_body_padding() {
    header_height = $('.main-header').innerHeight();
    if ($('.subpage-header').length) {
        header_height += $('.subpage-header').innerHeight();
    }
    $('body').css('padding-top', header_height);
}

function close_all() {
    $('#mobile-sidebar').hide("slide", {
        direction: "left"
    }, 500);
    $('.dim-bg-one').fadeOut(500);
    $('#search-close').click();
}
var didScroll = true;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header .main-header').outerHeight();

function hide_header_scroll() {
    $(document).scroll(function(event) {
        didScroll = true;
    });
    setInterval(function() {
        if (didScroll && $('.subpage-header').length && $('.is_desktop').is(":visible")) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);
}

function hasScrolled() {
    var st = $(this).scrollTop();
    if (Math.abs(lastScrollTop - st) <= delta)
        return;
    if (st > lastScrollTop && st > navbarHeight) {
        $('header .main-header').slideUp();
    } else {
        if (st + $(window).height() < $(document).height()) {
            $('header .main-header').slideDown();
        }
    }
    lastScrollTop = st;
}
/* ==============================================
========== End module 1 [header] ===========
============================================== */

/* ==============================================
========== module 3 [main-slider] ===========
============================================== */
$(document).ready(function() {
    var has_nav_slide = '';
    if ($('.thumbnail-slider').length) {
        has_nav_slide = '.thumbnail-slider';
    }
    $('.hero-slider').on('init', function(event, slick) {
        setTimeout(function() {
            push_slide_on_video($('.hero-slider'));
        }, 2500);
    });
    $('.hero-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        asNavFor: has_nav_slide,
        responsive: [{
            breakpoint: 1200,
            settings: {
                arrows: false,
            }
        }]
    });
    $('.thumbnail-slider').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.hero-slider',
        focusOnSelect: true,
        arrows: false,
    });
})
/* ==============================================
========== End module 3 [main-slider] ===========
============================================== */

/* ==============================================
========== module 4 [channels] ===========
============================================== */
function jk_fixed_slider_width() {
    var a = 0;
    $.when($('.channel-slider .item').each(function() {
        a += $(this).outerWidth(true);
    })).then($('.channel-slider .slick-track').css({
        'max-width': a + 20
    }));
}
$(document).ready(function() {
    $('.channel-slider').on('init', function(event, slick) {
        jk_fixed_slider_width();
    });
    $('.channel-slider').slick({
        dots: false,
        variableWidth: true,
        speed: 500,
        infinite: false,
        slidesToShow: 11,
        slidesToScroll: 11,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 6,
                dots: true,
                arrows: false
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                arrows: false
            }
        }]
    });
    $(".channel-slider .slick-slide img").hover(function(e) {
        var $this = $(this);
        if (e.type == "mouseenter") {
            var graySrc = $this.attr('src');
            var colorSrc = $this.data('color-img');
            $this.attr('src', colorSrc);
            $this.data('color-img', graySrc);
        } else {
            var colorSrc = $this.attr('src');
            var graySrc = $this.data('color-img');
            $this.attr('src', graySrc);
            $this.data('color-img', colorSrc);
        }
    });
    $(window).resize(function() {
        jk_fixed_slider_width();
    })
});
$(window).load(function() {
    jk_fixed_slider_width();
})
/* ==============================================
========== End module 4 [channels] ===========
============================================== */

/* ==============================================
========== module 8 [right-sidebar] ===========
============================================== */
$(document).ready(function() {
    $('.jk-widget4 .news-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
        arrows: false,
        dots: true,
        vertical: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                vertical: false,
            }
        }, ],
    });
    $('.item-slider-vrti').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        arrows: true,
        dots: false,
        prevArrow: '<span class="left"><i class="fa fa-chevron-left" aria-hidden="true"></i> PREV</span>',
        nextArrow: '<span class="right">NEXT <i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
    });
    $('.item-slider-vrti .title').on('click', function(even) {
        var $this = $(this);
        var $this_pre = $this.parents('.item-slider-vrti');
        if ($this.hasClass('opend')) {
            $this.removeClass('opend');
            $this.next('.text').slideUp();
            return;
        }
        $this_pre.find('.opend').removeClass('opend');
        $this_pre.find('.text').slideUp();
        $this.addClass('opend');
        $this.next('.text').slideDown(400);
    });
    $ticket_slider = $('.jk-widget9 .all-ticket');
    setting_ticket = {
        lazyLoad: 'progressive',
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 1199,
            settings: "unslick",
        }, ]
    }
    $ticket_slider.slick(setting_ticket);
    $(window).on('resize', function() {
        if ($('.is_desktop').is(':visible')) {
            if ($ticket_slider.hasClass('slick-initialized')) {
                $ticket_slider.slick('unslick');
            }
            return
        }
        if (!$ticket_slider.hasClass('slick-initialized')) {
            return $ticket_slider.slick(setting_ticket);
        }
    });
    $theaters_slider = $('.theaters-slider');
    theaters_slider_set = {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                adaptiveHeight: false,
                prevArrow: '<span class="left"><i class="fa fa-chevron-left" aria-hidden="true"></i> PREV</span>',
                nextArrow: '<span class="right">NEXT <i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
            }
        }, {
            breakpoint: 1199,
            settings: "unslick",
        }, ]
    }
    $theaters_slider.slick(theaters_slider_set);
    $(window).on('resize', function() {
        setTimeout(function() {
            custom_scrollbar_slider('.theaters-slider');
        }, 20);
        if ($('.is_desktop').is(':visible')) {
            if ($theaters_slider.hasClass('slick-initialized')) {
                $theaters_slider.slick('unslick');
            }
            return
        }
        if (!$theaters_slider.hasClass('slick-initialized')) {
            return $theaters_slider.slick(theaters_slider_set);
        }
    });
    custom_scrollbar_slider('.theaters-slider');
    $theaters_slider = $('#small-events');
    theaters_slider_set = {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        mobileFirst: true,
        dots: true,
        arrows: false,
        responsive: [{
            breakpoint: 1,
            settings: "unslick",
        }, {
            breakpoint: 767,
            settings: {
                adaptiveHeight: true,
            }
        }, {
            breakpoint: 1199,
            settings: "unslick",
        }, ]
    }
    $theaters_slider.slick(theaters_slider_set);
    $(window).on('resize', function() {
        setTimeout(function() {
            custom_scrollbar_slider('#small-events');
        }, 20);
        if ($('.is_desktop').is(':visible')) {
            if ($theaters_slider.hasClass('slick-initialized')) {
                $theaters_slider.slick('unslick');
            }
            return
        }
        if (!$theaters_slider.hasClass('slick-initialized')) {
            return $theaters_slider.slick(theaters_slider_set);
        }
    });
})

function make_same_height_slider($a) {
    var maxHeight = -1;
    $($a).each(function() {
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
        }
    });
    $($a).each(function() {
        if ($(this).height() < maxHeight) {
            $(this).css('margin-bottom', Math.ceil(maxHeight - $(this).height()) + 'px');
        }
    });
}

function custom_scrollbar_slider($container) {
    if ($('.is_desktop').is(':visible')) {
        $($container).niceScroll({
            autohidemode: false,
            cursorwidth: "10px",
            background: "#eee",
        });
    } else {
        $($container).getNiceScroll().remove();
    }
    if ($('.is_desktop').is(':visible')) {
        var ther_container = $('.theaters-slider');
        ther_container.each(function() {
            var $this = $(this);
            var sum = 0;
            var i = 0;
            $this.find('>div').each(function() {
                if (i++ < 3) {
                    sum += $(this).height();
                }
            });
            jk_next_prev_slide($this);
            $this.css('height', sum);
        });
    } else {
        $('.theaters-slider').css('height', 'auto');
    }
}

function jk_next_prev_slide($container) {
    var i = 0;
    var sbar = $container.offset().top + $container.scrollTop();
    $container.find('>div').each(function() {
        $this = $(this);
        div_pos = $this.offset().top;
        if (div_pos >= sbar && i == 0) {
            $this.addClass('active');
            i = 1;
        } else {
            $this.removeClass('active');
        }
    });
}
/* ==============================================
========== End module 8 [right-sidebar] ===========
============================================== */

/* ==============================================
========== module 9 [thumb-slider] ===========
============================================== */
$(document).ready(function() {
    $('.thumb-slider.show4').slick({
        arrows: true,
        lazyLoad: 'ondemand',
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: false,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            }
        }, {
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true,
                slidesToShow: 1.7,
                slidesToScroll: 1
            }
        }]
    });
    $('.thumb-slider.show3').slick({
        arrows: true,
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true,
                slidesToShow: 1.7,
                slidesToScroll: 1
            }
        }]
    });
    $('.thumb-slider.show2').slick({
        arrows: true,
        lazyLoad: 'ondemand',
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true,
                slidesToShow: 1.7,
                slidesToScroll: 1
            }
        }]
    });
    $('.thumb-slider.show1').slick({
        arrows: true,
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true,
            }
        }]
    });
    $slick_slider_show3 = $('.thumb-slider.show3_nomobile');
    setting_show3 = {
        arrows: true,
        lazyLoad: 'progressive',
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
        prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        responsive: [{
            breakpoint: 768,
            settings: "unslick",
        }, ]
    }
    $slick_slider_show3.slick(setting_show3);
    $(window).on('resize', function() {
        if ($('.is_mobile').is(':visible')) {
            if ($slick_slider_show3.hasClass('slick-initialized')) {
                $slick_slider_show3.slick('unslick');
            }
            return
        }
        if (!$slick_slider_show3.hasClass('slick-initialized')) {
            return $slick_slider_show3.slick(setting_show3);
        }
    });
    $show3_nodesktop = $('.thumb-slider.show3_nodesktop');
    setting_show3_ndesk = {
        lazyLoad: 'progressive',
        slidesToShow: 1.7,
        slidesToScroll: 1,
        infinite: false,
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 767,
            settings: "unslick",
        }, ]
    }
    $show3_nodesktop.slick(setting_show3_ndesk);
    $(window).on('resize', function() {
        if (!$('.is_mobile').is(':visible')) {
            if ($show3_nodesktop.hasClass('slick-initialized')) {
                $show3_nodesktop.slick('unslick');
            }
            return
        }
        if (!$show3_nodesktop.hasClass('slick-initialized')) {
            return $show3_nodesktop.slick(setting_show3_ndesk);
        }
    });
    $('.thumb-slider').on('lazyLoaded', function(event, slick, image, imageSource) {
        set_thumb_slider_arrow($(this));
        $('.thumb-slider').on('setPosition', function(slick) {
            set_thumb_slider_arrow($(this));
        });
    });
    set_video_play_button();
})

function set_thumb_slider_arrow($this) {
    var a = $this.find('.slick-active img').height();
    $this.find('.slick-arrow').css({
        'height': a,
        'visibility': 'visible',
        'padding-top': (a / 2) - 18,
    });
}

function set_video_play_button() {
    $('.jk-video img').wrap('<div class="icon-play"></div>');
}
/* ==============================================
========== End module 9 [thumb-slider] ===========
============================================== */

/* ==============================================
========== module 17 [iptv-popup] ===========
============================================== */
$(document).ready(function() {
    $('.product_popup').on('shown.bs.modal', function(e) {
        $('.product_mslider').slick({
            infinite: false,
            nav: true,
            dots: true,
            adaptiveHeight: true,
            prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        });
    });
})
/* ==============================================
========== End module 17 [iptv-popup] ===========
============================================== */

/* ==============================================
========== module 18 [list-filter] ===========
============================================== */
$(document).ready(function() {
    jk_html_search()
})

function jk_html_search() {
    var filter, tr, td, i;
    $('.list-filter-input').keyup(function() {
        tr = $(this).parents('.list-filter').find('li');
        if (!tr.length) {
            return;
        }
        filter = $(this).val().toUpperCase();
        for (i = 0; i < tr.length; i++) {
            td = tr[i];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
}
/* ==============================================
========== End module 18 [list-filter] ===========
============================================== */

/* ==============================================
========== module 19 [tab-vertical] ===========
============================================== */
$(document).ready(function() {
    $('.jk_tab_list li a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('.jk_tab_select').on('change', function(e) {
        $('.jk_tab_list li a').eq($(this).val()).click();
    });
})
/* ==============================================
========== End module 19 [tab-vertical] ===========
============================================== */

/* ==============================================
========== module 20 [popup-gallery] ===========
============================================== */
$(document).ready(function() {
    window.add_gallery_to_popup = function($this) {
        $this.parents('.popup-gall-parent').find('.thumb-slider.gall1').slick({
            arrows: true,
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                }
            }]
        });
        $this.parents('.popup-gall-parent').find('.thumb-slider.gall4').slick({
            arrows: true,
            lazyLoad: 'ondemand',
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: false,
            prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            responsive: [{
                breakpoint: 1050,
                settings: {
                    slidesToShow: 3.5,
                    slidesToScroll: 3,
                }
            }, {
                breakpoint: 890,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            }, {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2.5,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1.7,
                    slidesToScroll: 1
                }
            }]
        });
        $(document).on('click', '.thumb-slider.gallery .slick-slide', function() {
            $(this).find('.title').toggleClass('explore');
        });
        $('.thumb-slider').on('lazyLoaded', function(event, slick, image, imageSource) {
            set_thumb_slider_arrow($(this));
        });
        $('.thumb-slider').on('setPosition', function(slick) {
            set_thumb_slider_arrow($(this));
        });
    }
    $(document).on('click', '.thumb-slider.gallery_nav2 .slick-slide', function() {
        var $this = $(this);
        var slideIndex = $(this).index();
        $this.parents('.popup-gall-parent').find('.thumb-slider.gallery').slick('slickGoTo', slideIndex);
        $this.parents('.popup-gall-parent').find('.gallery_nav1.slick-initialized').slick('slickGoTo', slideIndex);
    });
    $(document).on('afterChange', '.thumb-slider.gallery', function(event, slick, currentSlide, nextSlide) {
        var $this = $(this);
        $this.parents('.popup-gall-parent').find('.gallery_nav2').slick('slickGoTo', currentSlide);
        $this.parents('.popup-gall-parent').find('.gallery_nav2 .slick-slide').removeClass('active');
        $this.parents('.popup-gall-parent').find('.gallery_nav2 .slick-slide[data-slick-index="' + currentSlide + '"]').addClass('active');
        $('iframe').each(function() {
            $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
        });
    });
    $('.movie-gallery').on('hidden.bs.modal', function() {
       // $(this).find(".jpopup").empty();
    });
})
/* ==============================================
========== End module 20 [popup-gallery] ===========
============================================== */

/* ==============================================
========== module 22 [tv-guide calendar] ========
============================================== */
$(document).ready(function() {
    var tv_active_slide_num, tv_desk_slide_num;

    function jk_set_first_slide() {
        var a = $('#tv-date-slider').find('.slick-slide.active');
        tv_active_slide_num = $('#tv-date-slider .slick-slide').index(a);
        if (tv_active_slide_num > 0) {
            tv_desk_slide_num = Math.floor(tv_active_slide_num / 7);
            tv_desk_slide_num *= 7;
        } else {
            tv_desk_slide_num = 0;
        }
    }
    $.when(jk_set_first_slide()).then(function() {
        $('#tv-date-slider').slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            initialSlide: 7,
            infinite: false,
            arrows: true,
            prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 5,
                    centerMode: true,
                    infinite: true,
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    centerMode: true,
                    infinite: true,
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    infinite: true,
                }
            }]
        });
        if ($('.is_mobile').is(':visible')) {
            $('#tv-date-slider').slick('slickGoTo', tv_active_slide_num);
        } else {
            $('#tv-date-slider').slick('slickGoTo', tv_desk_slide_num);
        }
    });
    $(window).resize(function() {
        setTimeout(function() {
            if ($('.is_mobile').is(':visible')) {
                $('#tv-date-slider').slick('slickGoTo', tv_active_slide_num);
            } else {
                $('#tv-date-slider').slick('slickGoTo', tv_desk_slide_num);
            }
        }, 800);
    })
})
/* ==============================================
======= End module 22 [tv-guide calendar] =======
============================================== */


/* ==============================================
========== page [contact] ===========
============================================== */
$(document).ready(function() {
    $('.simple-form').each(function() {
        var $this = $(this);
        $this.parsley().on('field:validated', function() {
            var ok = $this.find('.parsley-error').length === 0;
            console.log(ok);
            $this.find('.bs-callout-warning').toggleClass('hidden', ok);
            $this.find('.bs-callout-info').addClass('hidden');
        }).on('form:submit', function() {
            var ok = $this.find('.parsley-error').length === 0;
            if (ok) {
                $this.find('.bs-callout-info').removeClass('hidden');
                $this.parsley().reset();
            }
            return false;
        });
        $('.bs-callout .fa-times').click(function() {
            $(this).parent().addClass('hidden');
        });
    })
    $('.simple-form input[type=submit]').click(function() {
        $('.bs-callout-warning').addClass('hidden');
        $('.bs-callout-info').addClass('hidden');
    });
})
/* ==============================================
========== end page [contact] ===========
============================================== */

/* ==============================================
========== page [product-iptv-form] ===========
============================================== */
$(document).ready(function() {
    if ($('#form-iptv').length) {
        $('#form-iptv').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-warning').toggleClass('hidden', ok);
            $('.bs-callout-info').addClass('hidden');
        }).on('form:submit', function() {
            var ok = $('.parsley-error').length === 0;
            if (ok) {
                $('.bs-callout-info').removeClass('hidden');
            }
            return false;
        });
        $('.bs-callout .fa-times').click(function() {
            $(this).parent().addClass('hidden');
        });
    }
    var $sections = $('.from-section');

    function navigateTo(index) {
        $('.form-navigation .jk-current').text(index + 1);
        $sections.removeClass('current').eq(index).addClass('current');
        $('.form-navigation .previous').toggle(index > 0);
        var atTheEnd = index >= $sections.length - 1;
        $('.form-navigation .next').toggle(!atTheEnd);
        $('.form-navigation [type=submit]').toggle(atTheEnd);
    }

    function curIndex() {
        return $sections.index($sections.filter('.current'));
    }
    $('.form-navigation .previous').click(function() {
        navigateTo(curIndex() - 1);
    });
    $('.form-navigation .next').click(function() {
        if ($('#form-iptv').parsley().validate({
                group: 'block-' + curIndex()
            }))
            navigateTo(curIndex() + 1);
    });
    $sections.each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });
    navigateTo(0);
    $('.form-navigation .jk-all').text($sections.length);
    select_iptv_packages();
    disable_billing_address();
    $('#shipping_as_billing').on('click', function(event) {
        disable_billing_address();
    })
})

function disable_billing_address() {
    if ($('#shipping_as_billing').is(":checked")) {
        $('#all-billing-fields').addClass('disabled');
        $('#all-billing-fields').find('input').prop('disabled', true);
    } else {
        $('#all-billing-fields').removeClass('disabled');
        $('#all-billing-fields').find('input').prop('disabled', false);
    }
}

function select_iptv_packages() {
    $('.select-package').click(function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        var packid = $(this).data("packid");
        try {
            localStorage.iptv_packId = packid;
        } catch (e) {}
        location.href = url;
    });
    if ($('#iptv-package').length && localStorage.iptv_packId) {
        $('#iptv-package').val(localStorage.iptv_packId);
    }
}
/* ==============================================
========== end page [product-iptv-form] ===========
============================================== */

/* ==============================================
========== page [service-starkargo] ===========
============================================== */
/* ================= prohibited popup ================ */
$(document).ready(function() {
    $mobile_nodesktop = $('.thumb-slider.mobile_nodesktop');
    setting_mobile_nodesktop = {
        lazyLoad: 'progressive',
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 1199,
            settings: "unslick",
        }, ]
    }
    $('#starkargo_prohibited').on('shown.bs.modal', function() {
        $mobile_nodesktop.slick(setting_mobile_nodesktop);
    });
    $('#starkargo_prohibited').on('hidden.bs.modal', function() {
        $mobile_nodesktop.slick('unslick');
    });
    $(window).on('resize', function() {
        if ($('.is_desktop').is(':visible')) {
            if ($mobile_nodesktop.hasClass('slick-initialized')) {
                $mobile_nodesktop.slick('unslick');
            }
            return
        }
        if (!$mobile_nodesktop.hasClass('slick-initialized') && $('#starkargo_prohibited').hasClass('in')) {
            return $mobile_nodesktop.slick(setting_mobile_nodesktop);
        }
    });
})
$(window).on('resize', function() {
    if (!$('.is_desktop').is(':visible')) {
        $('#starkargo_prohibited').find('.slick-initialized').slick('slickGoTo', 1);
        setTimeout(function() {
            $('#starkargo_prohibited').find('.slick-initialized').slick('slickGoTo', 0);
        }, 500);
    }
})
/* ================= end prohibited popup ================ */

/* ================= FAQ popup ================ */
$(document).ready(function() {
    $('#starkargo_faq_button').mouseenter(function(event) {
        $('#starkargo_faq').modal('show');
    });
    $('.type_tooltip').on('show.bs.modal', function() {
        setTimeout(function() {
            $('.modal-backdrop').css('visibility', 'hidden');
        }, 10);
    });
    $('.type_tooltip').on('hidden.bs.modal', function() {
        $('.modal-backdrop').css('visibility', 'visible');
    });
}) // end document.ready
/* ================= end FAQ popup ================ */


/* ==============================================
========== end page [service-starkargo] ===========
============================================== */

/* ==== responsive facebook video.. ========= */
$(document).ready(function() {
    $('iframe').each(function() {
        $this = $(this);
        var a = $this.attr('src');
        if (a.lastIndexOf('facebook.com/plugins') > 0) {
            $this.addClass('embed-responsive-item');
            $this.wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
        }
    });
}) // .ready
/* ==== /responsive facebook video.. ========= */


/* === print === */
$(document).ready(function() {
    $("#print_btn").printPreview({
        obj2print: '#print_this',
        width: '810'
    });
    $('.main-nav-desktop li>a[href="#"]').click(function(event) {
        event.preventDefault();
    });
});

// show/hide the right sidebar of main content area..
$(document).ready(function() {
    $('.r-sidebar').each(function() {
        var $jk_right_side_txt = $(this).html();
        if ($jk_right_side_txt.trim() == '') {
            $(this).prev("div").removeClass('col-lg-8');
            $(this).prev("div").addClass('col-12');
        }
    });
    // hide sub-header if no content
    $('.subpage-header>.inner').each(function() {
        var a = $(this).html();
        if (a.trim() == '') {
            $(this).remove();
        }
    });
});
/* open external content on a popup */
$(document).ready(function() {
    $(document).on('click', '.on_popup', function(event) {
        event.preventDefault();
        $('#on_popup_html').remove();
        $('body').append('<div class="modal fade product_popup " id="on_popup_html" role="dialog"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><i aria-hidden="true" class="fa fa-times close" data-dismiss="modal">&nbsp;</i><h4 class="modal-title">Loading...</h4></div><div class="modal-body clearfix"><div class="col-sm-12">Loading...</div></div></div></div></div>');
        $('#on_popup_html').modal('show');
        var url = $(this).attr('href');
        $.get(url).success(function(data) {
            var cont, title, re;
            if ($(data).find('h1').length > 0) {
                title = $(data).find('h1').html();
                re = /(<h1[^>]*>((?:.|\r?\n)*?)<\/h1>)/i;
            } else {
                title = $(data).find('h2').html();
                re = /(<h2[^>]*>((?:.|\r?\n)*?)<\/h2>)/i;
            }
            if ($(data).find('#print_this').length > 0) {
                cont = $(data).find('#print_this').html();
            } else {
                cont = $(data).find('.content1').html();
            }
            cont = cont.replace(re, "");
            $('#on_popup_html .modal-title').html(title);
            $('#on_popup_html .modal-body>.col-sm-12').html(cont);
        });
    });
    $('.main-content .col-lg-8 table').wrap("<div class='table-responsive'></div>");
    $('.main-content .col-lg-8 table').addClass("table");
})
/* /open external content on a popup */

function init() {
    var vidDefer = document.getElementsByTagName('iframe');
    for (var i = 0; i < vidDefer.length; i++) {
        if (vidDefer[i].getAttribute('data-src')) {
            vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
        }
    }
}
window.onload = init;




// ======= on page js [these added after publish] move to here to make html clean.
$(document).ready(function(){
    jk_dispaly_disclaimer();  // tv-guide page disclaimer
    
    $('#tv_guide_time-zone').change(function (){
      jk_dispaly_disclaimer();  // tv-guide page disclaimer
    });

    jk_scroll_to_div(); // any link with #scroll_ will scroll to a #id

    jk_open_popup_modal_cms_button_click();    // some place on cms we can't add modal attr, so we did this.

    jk_play_popup_video_movie(); // play popup video on movie page uisng modal

    jk_push_slide_on_video_change();

    jk_starkargo_agent_drop();  // starkargo agent dropdown

    scroll_to_section(); // scroll to div for legal pages only
    show_back_top(); // back to top
    
    jk_full_video_bg_page(); // full width video/image background page 

    jk_video_on_hero_slider();  // if any hero slide have youtube video

    gallery_open_on_page_load();   // open popup gallery on page load

    jk_gtm_datalayer_push(); // google datalayer data push

    // set popup gallery height 
    $('.modal.movie-gallery').on('show.bs.modal', function (e) {
        set_gallery_width($(this));
    }); // set popup gallery height...


    $(document).on('click', '.InfoLabel, .ErrorLabel', function(){
        $(this).hide();
    }); // hide default kentico form's info box
    
}) // end doc.ready

function jk_dispaly_disclaimer(){     // tv-guide page disclaimer
    var jk_cnlName= $("[id$='TvGuideChannels_ssfChannels_drpFilter'] option:selected").text();      
    var jk_tz= $("#tv_guide_time-zone option:selected").val();      
    
    var jk_tz_box = $('#tv_guide_time-zone');
    
    if(jk_cnlName == "The Filipino Channel"){
        
        $('#tv_guide_time-zone option').each(function(){
            if($(this).val() == 'ET'){
                $(this).text('ET*');
            }
        }) // change dropdown value
        
        if(jk_tz == 'ET'){ // show / hide the message
            jk_tz_box.parents('.row').find('.jk_disclaimer').fadeIn();
        }else{
            jk_tz_box.parents('.row').find('.jk_disclaimer').fadeOut();
        }
    }
} // jk_dispaly_disclaimer ... 



function jk_scroll_to_div(){  // any link with #scroll_ will scroll to a #id
    $("a[href^='#scroll_']").click(function(event){
        event.preventDefault();
        var id = $(this).attr('href');
        var ids = id.replace("scroll_", "");
        
        $('html, body').animate({
            scrollTop: $(ids).offset().top - 70
        }, 1000);
        
    });
}  // jk_scroll_to_div.... any link with #scroll_ will scroll to a #id

function jk_open_popup_modal_cms_button_click(){    // some place on cms we can't add modal attr, so we did this.
    $("a[href^='#popup_']").click(function(event){
    event.preventDefault();
    var id= $(this).attr('href');
    var ids= id.replace("popup_", "");
    
    $(ids).modal('show');
    
    }); // show modal from slider button
} // jk_open_popup_modal_cms_button_click

function jk_play_popup_video_movie(){ // popup video/image gallery
    $(".thumbvideo").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        
        $this.parents('.thumb').append('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
        
        // $this.parents('.popup-gall-parent').find(".jpopup").empty();
        
        $this.parents('.popup-gall-parent').find('.movie-gallery').one('shown.bs.modal', function () {
            //$this.parents('.popup-gall-parent').find( '.thumb-slider.gallery' ).slick('slickGoTo', 0);
            //$this.parents('.popup-gall-parent').find( '.gallery_nav2' ).slick('slickGoTo', 0);
            $(window).trigger('resize');
            
            setTimeout(function(){ // to insure slider already in place.
                $this.parents('.popup-gall-parent').find( '.thumb-slider.gallery, .thumb-slider.gallery_nav2' ).css('visibility', 'visible');
                
                $this.parents('.thumb').find('.fa').remove();
            }, 500);
        });
        $this.parents('.popup-gall-parent').find('.movie-gallery').modal('show');
        
        /* var sectiondata = $this.data("url");
        $this.parents('.popup-gall-parent').find(".jpopup").load("/Old_App_Code/CMSModules/ABSCMS/videopopup.aspx", { "section":sectiondata }, function(){
            $.when(add_gallery_to_popup($this)).then(function(){
                $this.parents('.popup-gall-parent').find('.movie-gallery').modal('show');
            });
        });    */     
    });  // end click 
} // jk_play_popup_video_movie  ... popup video/image gallery

function jk_push_slide_on_video_change(){
    //push_slide_on_video();
    $('.hero-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
        push_slide_on_video($(this));
    });
}
function push_slide_on_video($this){
    if($this.find('.slick-current .jk_vid_bg_hero_cnt').length>0){
        $this.slick('slickPause');
    }else{
        $this.slick('slickPlay');
    }
}
// jk_push_slide_on_video_change

function jk_starkargo_agent_drop(){  // starkargo agent drop down
    $('select.agent-select-tabs').on('change', function () {
        $(':selected', this).tab('show');
       // alert($(':selected', this));
    });
}


function scroll_to_section() { // scroll to element for legal page only
    $(".main-content.page-terms a[href^='#']").click(function(event) {
        event.preventDefault();
        var a = $(this).attr('href');
        
        $('html, body').animate({
            scrollTop: $(a).offset().top - 50
        }, 500);
    });
} // scroll_to_section ... for legal page only


function show_back_top() {   // back to top button 
    $(document).scroll(function() {
      var a = $(this).scrollTop();
      if (a > 500) {
        $('#back_top').addClass('show');
      } else {
        $('#back_top').removeClass('show');
      }
    });
} // show_back_top


function jk_full_video_bg_page(){ // full width video/image background page 
    var vdo_id = $.trim($('#pagebackgroundvideo').val());
    if(vdo_id && vdo_id != ""){
        $('#background-video').YTPlayer({
          videoId: vdo_id,
          repeat: true,
          events: {
              'onStateChange': onPlayerStateChange,
            }

        });

        function onPlayerStateChange(event){
            if(event.data==YT.PlayerState.PLAYING){
                $('#background-video1').remove();
            }
            if(event.data==YT.PlayerState.ENDED){
                event.target.playVideo();
            }
        }

       
    }

    var jk_vd_pg_h = $( window ).innerHeight();
    var jk_header_h = $('header').innerHeight();
    $('.jk_vdi_bg>.container').css({'min-height': jk_vd_pg_h - jk_header_h });

    $(window).resize(function(){
        var jk_vd_pg_h = $( window ).innerHeight();
        var jk_header_h = $('header').innerHeight();
        $('.jk_vdi_bg>.container').css({'min-height': jk_vd_pg_h - jk_header_h });
    }); // widdow.resize
} // jk_full_video_bg_page ... full width video/image background page 


function jk_video_on_hero_slider(){  // if any hero slide have youtube video
    $('.jk_vid_bg_hero_cnt').each(function(){
        var $this= $(this);
        var jk_vid_hero= $this.data('id');
        var jk_vid_hero1= jk_vid_hero.replace("jk_vid_", "");
        console.log(jk_vid_hero);
        
        $this.find('.jk_vid_bg_hero').YTPlayer({
        videoId: jk_vid_hero1,
        repeat: true,
        events: {
          'onStateChange': function(event){
            if(event.data==YT.PlayerState.PLAYING){
            $this.find('.bg').remove();
        }
         if(event.data==YT.PlayerState.ENDED){
            event.target.playVideo();
        }
          } // onStateChange
        }
        
        });
    }); //.jk_vid_bg_hero_cnt
}  //jk_video_on_hero_slider ... if any hero slide have youtube video

function gallery_open_on_page_load(){   // open popup gallery on page load
    var url= $(location).attr("href");
        var a= url.lastIndexOf('#gallery_');
    var wait= 500;
    if(!(a > 0)){
    return;
    }
        a= url.substring(a);
        console.log(a);
        var b= a.replace("#gallery_", "#jk_gallery_");
        
        if($(b).length){
          window.setTimeout( function(){
               $(b)[0].click();
           }, wait ); 
        }
    return;
} //gallery_open_on_page_load ... open popup gallery on page load


function set_gallery_width($this) {  // set popup gallery height
	var win_h = $(window).innerHeight();
	var margin_top = 160;
	
	if ($('.is_mobile').is(':visible')) { // becasue on mobile we don't have margin-top
		margin_top-= 100;
	}

	
	if($this.find('.modal-footer .slick-slide').length > 0 && win_h >= 550){ 
		if ($('.is_desktop').is(':visible')) {
			margin_top+= 200;
		} else if ($('.is_tab').is(':visible')) {
			margin_top+= 130;
		}
	}
    
    if($this.find('.modal-footer .slick-slide').length <= 0){
      $this.find('.modal-footer').css({'padding':'5px'});
      $this.find('.gallery_nav2').remove();
    }
	

	var max_w = (win_h - margin_top) * 1.78;
	if(max_w < 1200){
		$this.find('.thumb-slider.gallery').css({'width' : max_w});
	}
} // end set popup gallery height func.?


function jk_gtm_datalayer_push(){ // google datalayer data push
    $(document).on("click", ".jk_gtmdata", function(event){
        var $this = $(this);
        var jk_cat = $this.data('gtmcat');
        var jk_act = $this.data('gtmact');
        var jk_lab = $this.data('gtmlab');
        
        dataLayer.push({
         'tcategory': jk_cat,
         'taction': jk_act,
         'tlabel': jk_lab,
         'event': 'mytfc'
        });
    });
} // jk_gtm_datalayer_push ... google datalayer data push