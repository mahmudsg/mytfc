var $fl = 1;
var f2 = 1;
$(document).ready(function(){
	// mobile menu
	$('.mobile_button>button').click(function(e){
		$('.header_sec .main_menu').toggleClass('active');
	});
	$(document).click(function(e){
		var mobile_button = $('.mobile_button>button');  // hide the Menu on outside click
		if (!mobile_button.is(e.target) && mobile_button.has(e.target).length === 0){
			$('.header_sec .main_menu').removeClass('active');
		}
	}); // end mobile menu
	
	// === FAQs
	$('.faq_boxes h4').click(function(){
		if($(this).hasClass('active')){
			$(this).next('.inner').slideUp(300);
			$(this).removeClass('active');
			return;
		}
		$('.faq_boxes h4').removeClass('active');
		$(this).addClass('active');
		$('.faq_boxes .inner').slideUp(300);
		$(this).next('.inner').slideDown(300);
	});
	
	// =============== smoothScroll
	var menu_height = $('.header_sec').outerHeight();
	//$('body').css({'padding-top':menu_height - 10});
	/* $('.main_menu a').smoothScroll({
		offset: -menu_height
	}); */
	
	$('.i_want').click(function(){
		$('html,body').animate({
        scrollTop: $("#contact_area").offset().top - menu_height});
	});
	
	// Cache selectors
	var lastId,
    topMenu = $("#main_menu"),
    topMenuHeight = menu_height,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    }),
    noScrollAction = false;

	// Bind click handler to menu items
	// so we can get a fancy scroll animation
	menuItems.click(function(e){
		e.preventDefault();
		var href = $(this).attr("href"),
			offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight;
		noScrollAction = true;
		$('html, body').stop().animate({ 
			scrollTop: offsetTop
		},{
			duration: 300,
			complete: function() {
				menuItems
					.parent().removeClass("active")
					.end().filter("[href=" + href +"]").parent().addClass("active");
				setTimeout(function(){ noScrollAction = false; }, 10);
			}
		});
		//e.preventDefault();
	});

	// Bind to scroll
	$(window).scroll(function(){
	   if(!noScrollAction){
		   // Get container scroll position
		   var fromTop = $(this).scrollTop() + topMenuHeight + 100;
		   
		   // Get id of current scroll item
		   var cur = scrollItems.map(function(){
			 if ($(this).offset().top < fromTop)
			   return this;
		   });
		   // Get the id of the current element
		   cur = cur[cur.length-1];
		   var id = cur && cur.length ? cur[0].id : "";
		   
		   if (lastId !== id) {
			   lastId = id;
			   // Set/remove active class
			   menuItems
				 .parent().removeClass("active")
				 .end().filter("[href=#"+id+"]").parent().addClass("active");
		   }
	   }    
	});
	
	// video pleayer 2017
	$('.video_outer img').click(function(){
		var src = $(this).data('src');
		$(this).fadeOut(0);
		$(this).parent().find('iframe').attr('src',src);
		$(this).parent().find('iframe').fadeIn(0);
	});
	
	// slider
	$('.one_slider').slick({
		dots: true,
		arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		adaptiveHeight: true
	});
	$('#package_area .mobile').slick({
		dots: true,
		arrows: true,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		adaptiveHeight: true
	});

	
	// ================================ contact form 
	$('.close').click(function(){
		$(this).parent().slideUp();
	});
	
	$('.checkbox_outer').click(function(){
		if($(this).find('input:checked').length > 0){
			$(this).find('input').val('added');
			$(this).find('.checkbox_over').css({'border-color':'#cbc9c9'});
		}else{
			$(this).find('input').val('');
			$(this).find('.checkbox_over').css({'border-color':'#ff0000'});
		}
		$(this).find('input').change();
	});
	
	// remove pink bg on keypress
	$(document).on('keyup change select','.required',function(){
		
		$this = $(this);
		if($this.val() != ""){
			if($this.hasClass('email') && !isValidEmailAddress($this.val())){
				$this.css({'border-color':'#ff0000'});
			}else if($this.hasClass('phone') && !isPhone($this.val())){
				$this.css({'border-color':'#ff0000'});
			}else{
				$this.css({'border-color':'#cbc9c9'});
			}
		}else{
			$this.css({'border-color':'#ff0000'});
		}
		
		// make empty email as not required
		if($this.val() == "" && $this.hasClass('email')){
			$this.removeClass('required');
			$this.css({'border-color':'#cbc9c9'});
		}
		
		// enable / disable send button
		if($fl == 0){
			var f2 = 1;
			$('.required').each(function(){
				$this = $(this);
				if($this.val() != ""){
					if($this.hasClass('email') && !isValidEmailAddress($this.val())){
						f2 = 0;
					}else if($this.hasClass('phone') && !isPhone($this.val())){
						f2 = 0;
					}
				}else{
					f2 = 0;
				}
			});
			
			if(f2 == 1){
				$('#SendSignUpForm').prop('disabled', false);
			}else{
				$('#SendSignUpForm').prop('disabled', true);
			}
		}
		
	});
	$('form input[type=email]').on('keyup change select',function(){
		$this = $(this);
		if($this.val() != ""){
			$this.addClass('required');
		}else{
			//$this.removeClass('required');
		}
	});
	
	// form submit
	$('#SendSignUpForm').click(function(){
		// check input boxes
		$fl = 1;
		$('.required').each(function(){
			$this = $(this);
			if($this.val() != ""){
				if($this.hasClass('email') && !isValidEmailAddress($this.val())){
					$this.css({'border-color':'#ff0000'});
					$fl = 0;
				}else if($this.hasClass('phone') && !isPhone($this.val())){
					$this.css({'border-color':'#ff0000'});
					$fl = 0;
				}else{
					$this.css({'border-color':'#cbc9c9'});
				}
				if($this.hasClass('checkbox')){
					$this.next('.checkbox_over').css({'border-color':'#cbc9c9'});
				}
			}else{
				$this.css({'border-color':'#ff0000'});
				if($this.hasClass('checkbox')){
					$this.next('.checkbox_over').css({'border-color':'#ff0000'});
				}
				$fl = 0;
			}
		});
		
		if($fl){
			//$("#info_form_in").submit();
			/* $.ajax({
				type:'POST',			
				data: jQuery("#info_form_in").serialize(),
				url:'//aws.kapamilya.com/tfc-japan-promo/php/process.php',
				beforeSend: function() {
					$('#ajax_loder').show();
					$('#SendSignUpForm').prop('disabled', true);
					$('.form_msg').slideUp();
				},
				success:function(result) {
				  $('#ajax_loder').hide();
				  $('#info_form_in select, #info_form_in input, #info_form_in textarea').val('');
				  $('.form_msg.red, .form_msg.red2').slideUp();	
				  $('.form_msg.green').slideDown();	
				  backtoTop();
				  dataLayer.push({'event': 'form_submit_success'}); // GTM
				},
				error: function(request,status,errorThrown) {
					$('#SendSignUpForm').prop('disabled', false);
					$('.form_msg.green, .form_msg.red').slideUp();	
					$('.form_msg.red2').slideDown();
					$('.form_msg.red2 .error').html(errorThrown);
					backtoTop();
					dataLayer.push({'event': 'form_submit_fail'}); // GTM
				}
			}); */

			$('#ajax_loder').hide();
			$('#info_form_in select, #info_form_in input, #info_form_in textarea').val('');
			$('.form_msg.red, .form_msg.red2').slideUp();	
			$('.form_msg.green').slideDown();	
			backtoTop();
			dataLayer.push({'event': 'form_submit_success'}); // GTM
		}else{
			$('#SendSignUpForm').prop('disabled', true);
			$('.form_msg.green').slideUp();	
			$('.form_msg.red').slideDown();
			backtoTop();
		}
		
		
	});

});

function backtoTop(){
	$('html,body').animate({
	scrollTop: $("#left_top").offset().top-100},
	'slow');
}

// cehck mail validation
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

// check phone validation
function isPhone(phoneNum){
	var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
	return numberRegex.test(phoneNum);
}


 /// ======= new js
 $(window).load(function() {
	scroll_to_div_pageload(); // scroll to a div after page load, target from url
})// end windows.load


function scroll_to_div_pageload() {
	var url = $(location).attr("href");
	var a = url.lastIndexOf('#');
var wait = 0;
if(!(a > 0)){
	return;
}
	var a = url.substring(a);
	if(a == '#sendgrid_mc_email_subscribe'){
			a = '#sendgrid_nlvx_widget-2';
	wait = 500;
	}
//alert(a);
	if($(a).length){
	 window.setTimeout( function(){
		 $('html, body').animate({
			scrollTop: $(a).offset().top - 110
		}, 400);  
	 }, wait ); 
			
	}
return;
}// scroll to div pageload