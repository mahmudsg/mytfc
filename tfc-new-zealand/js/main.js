var $fl = 1;
var menu_height;
$(document).ready(function(){
	// mobile menu
	$('.mobile_button>button').click(function(e){
		$('.header_sec .main_menu').toggleClass('active');
	});
	$(document).click(function(e){
		var mobile_button = $('.mobile_button>button');  // hide the Menu on outside click
		if (!mobile_button.is(e.target) && mobile_button.has(e.target).length === 0){
			$('.header_sec .main_menu').removeClass('active');
		}
	}); // end mobile menu
	
	// === FAQs
	$('.faq_boxes h4').click(function(){
		if($(this).hasClass('active')){
			$(this).next('.inner').slideUp(300);
			$(this).removeClass('active');
			return;
		}
		$('.faq_boxes h4').removeClass('active');
		$(this).addClass('active');
		$('.faq_boxes .inner').slideUp(300);
		$(this).next('.inner').slideDown(300);
	});
	
	// =============== smoothScroll
	menu_height = $('.header_sec').outerHeight();
	//$('body').css({'padding-top':menu_height});
	/* $('.main_menu a').smoothScroll({
		offset: -menu_height
	}); */
	
	$('.i_want').click(function(){
		$('html,body').animate({
        scrollTop: $("#contact_area").offset().top - menu_height});
	});
	
	// Cache selectors
var lastId,
    topMenu = $("#main_menu"),
    topMenuHeight = menu_height,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    }),
    noScrollAction = false;

	// Bind click handler to menu items
	// so we can get a fancy scroll animation
	menuItems.click(function(e){
		e.preventDefault();
		var href = $(this).attr("href"),
			offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight;
		noScrollAction = true;
		$('html, body').stop().animate({ 
			scrollTop: offsetTop
		},{
			duration: 300,
			complete: function() {
				menuItems
					.parent().removeClass("active")
					.end().filter("[href=" + href +"]").parent().addClass("active");
				setTimeout(function(){ noScrollAction = false; }, 10);
			}
		});
		//e.preventDefault();
	});

	// Bind to scroll
	$(window).scroll(function(){
	   if(!noScrollAction){
		   // Get container scroll position
		   var fromTop = $(this).scrollTop()+topMenuHeight + 100;
		   
		   // Get id of current scroll item
		   var cur = scrollItems.map(function(){
			 if ($(this).offset().top < fromTop)
			   return this;
		   });
		   // Get the id of the current element
		   cur = cur[cur.length-1];
		   var id = cur && cur.length ? cur[0].id : "";
		   
		   if (lastId !== id) {
			   lastId = id;
			   // Set/remove active class
			   menuItems
				 .parent().removeClass("active")
				 .end().filter("[href=#"+id+"]").parent().addClass("active");
		   }
	   }    
	});

	
	// ================================ contact form 
	$('.close').click(function(){
		$(this).parent().slideUp();
	});
	// remove pink bg on keypress
	$('.required').on('keyup change select',function(){
		$this = $(this);
		if($.trim($this.val()) != ""){
			if($this.hasClass('email') && !isValidEmailAddress($this.val())){
				$this.css({'border-color':'#ff0000'});
			}else if($this.hasClass('phone') && !isPhone($this.val())){
				$this.css({'border-color':'#ff0000'});
			}else{
				$this.css({'border-color':'#cbc9c9'});
			}
		}else{
			$this.css({'border-color':'#ff0000'});
		}
		
		// enable / disable send button
		if($fl == 0){
			var f2 = 1;
			$('.required').each(function(){
				$this = $(this);
				if($this.val() != ""){
					if($this.hasClass('email') && !isValidEmailAddress($this.val())){
						f2 = 0;
					}else if($this.hasClass('phone') && !isPhone($this.val())){
						f2 = 0;
					}
				}else{
					f2 = 0;
				}
			});
			
			if(f2 == 1){
				$('#info_form_submit').prop('disabled', false);
			}else{
				$('#info_form_submit').prop('disabled', true);
			}
		}
		
	});
	
	// form submit
	$('#info_form_submit').click(function(){
		// check input boxes
		$fl = 1;
		$('.required').each(function(){
			$this = $(this);
			if($.trim($this.val()) != ""){
				if($this.hasClass('email') && !isValidEmailAddress($this.val())){
					$this.css({'border-color':'#ff0000'});
					$fl = 0;
				}else if($this.hasClass('phone') && !isPhone($this.val())){
					$this.css({'border-color':'#ff0000'});
					$fl = 0;
				}else{
					$this.css({'border-color':'#cbc9c9'});
				}
			}else{
			$this.css({'border-color':'#ff0000'});
				$fl = 0;
			}
		});
		
		if($fl){
			//$("#info_form_in").submit();
			/* $.ajax({
				type:'POST',
				data: jQuery("#left_top").find("input, select, textarea").serialize(),
				url:'http://aws.kapamilya.com/tfc-new-zealand/php/process.php',
				beforeSend: function() {
					$('#ajax_loder').show();
					$('#info_form_submit').prop('disabled', true);
					$('.form_msg').slideUp();
				},
				success:function(result) {
				  $('#ajax_loder').hide();
				  $('#left_top select, #left_top input[type=text], #left_top input[type=email], #left_top textarea').val('');
				  $('.form_msg.red, .form_msg.red2').slideUp();	
				  $('.form_msg.green').slideDown();	
				  backtoTop();
				  dataLayer.push({'event': 'form_submit_success'});  // GTM
				},
				error: function(request,status,errorThrown) {
					$('#info_form_submit').prop('disabled', false);
					$('.form_msg.green, .form_msg.red').slideUp();	
					$('.form_msg.red2').slideDown();
					$('.form_msg.red2 .error').html(errorThrown);
					backtoTop();
					dataLayer.push({'event': 'form_submit_fail'});  // GTM
				}
			}); */
				
				$('#ajax_loder').hide();
				$('#left_top select, #left_top input[type=text], #left_top input[type=email], #left_top textarea').val('');
				$('.form_msg.red, .form_msg.red2').slideUp();	
				$('.form_msg.green').slideDown();	
				backtoTop();
				dataLayer.push({'event': 'form_submit_success'});
		}else{
			$('#info_form_submit').prop('disabled', true);
			$('.form_msg.green').slideUp();	
			$('.form_msg.red').slideDown();
			backtoTop();
		}
		
		
	});
	
	
	/* ======================= sliders ================== */
    if($('.mobile_slider').length > 0){
    	$show3_nodesktop = $('.mobile_slider');
	    setting_show3_ndesk = {
	        lazyLoad: 'progressive',
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        infinite: false,
	        mobileFirst: true,
	        dots:true,
	        arrows: false,
	        adaptiveHeight: true,
	        responsive: [
	        {
	          breakpoint: 768,
	          settings: "unslick",
	        },
	        ]
	    }
	    $show3_nodesktop.slick(setting_show3_ndesk);
	    
	    
	    // reslick only if it's not slick()
	    $(window).on('resize', function() {
	        if (!$('.is_mobile').is(':visible')) {
	          if ($show3_nodesktop.hasClass('slick-initialized')) {
	            $show3_nodesktop.slick('unslick');
	          }
	          return
	        }
	        
	        if (!$show3_nodesktop.hasClass('slick-initialized')) {
	          return $show3_nodesktop.slick(setting_show3_ndesk);
	        }
	        
	        
	    });
		/* /======================= sliders ================== */
    } // /if has slider
	
	$(window).resize(function () {
		menu_height = $('.header_sec').outerHeight();
		topMenuHeight = menu_height;
	});

});

function backtoTop(){
	return;
	$('html,body').animate({
	scrollTop: $("#left_top").offset().top-100},
	'slow');
}

// cehck mail validation
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

// check phone validation
function isPhone(phoneNum){
	var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
	return numberRegex.test(phoneNum);
}